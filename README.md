# Probalities of Terminal Conditions

The source code provides optimal policy, expected value, and probability of each outcome for a card game where there are a known number of red and black cards in a deck.  The player will win +$1 for each red card flipped and -$1 for each black card flipped.  The player can flip as many cards as desired.  

The probabilies for all states of the game with optimal play were calculated for decks with even numbers of red and black cards ranging from 5 to 130 (5 decks) of each card.

The plots below show the probabilities of each state of the game.  Red font states show states which are terminal for optimal play as well as the winnings associtated with those states.


## 5
![one](src/imgs/heatmap/5_cards.png)
![one](src/imgs/bar/5_cards.png)

## 8
![one](src/imgs/heatmap/8_cards.png)
![one](src/imgs/bar/8_cards.png)

## 13
![one](src/imgs/heatmap/13_cards.png)
![one](src/imgs/bar/13_cards.png)

## 26
![one](src/imgs/heatmap/26_cards.png)
![one](src/imgs/bar/26_cards.png)

## 52
![one](src/imgs/heatmap/52_cards.png)
![one](src/imgs/bar/52_cards.png)

## 130
![one](src/imgs/heatmap/130_cards.png)
![one](src/imgs/bar/130_cards.png)
