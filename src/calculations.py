#%%
import os
import pickle
from time import time

import matplotlib.pyplot as plt
import numpy as np
from matplotlib import rcParams

plt.style.use("default")
rcParams["font.family"] = "serif"

EXPECTED_VALUES = {}

def expected_value(n_reds, n_blacks):
    """ Recursively updates the expected value of a given state of the game.

    Assumes negative expected valued games will not be played.

    Inputs:
        n_reds: Integer number of red cards remaining in deck
        n_blacks: Integer number of black cards remaining in deck

    Returns:
        expected value of the given state of game
        Updated EXPECTED_VALUES dictionary where
            keys: tuple of game state (n_reds, n_blacks)
            values: expected value of game state with optimal play
    """
    
    test_key = (n_reds, n_blacks)

    if test_key in EXPECTED_VALUES:
        e_val = EXPECTED_VALUES[(n_reds, n_blacks)]
    else:
        n_tot = n_reds + n_blacks

        e_val = 0
        
        if n_reds > 0:
            e_red = 1 + expected_value(n_reds - 1, n_blacks)
            e_val += (n_reds / n_tot) * e_red

        if n_blacks > 0:
            e_black = -1 + expected_value(n_reds, n_blacks - 1)
            e_val += (n_blacks / n_tot) * e_black
            
        if e_val < 0:
            e_val = 0

        EXPECTED_VALUES.update({test_key: e_val})
    return e_val

def determine_optimal_play(n_reds:int, n_blacks:int) -> dict:
    """ Determines optimal policy

    Inputs:
    n_reds: Integer number of red cards remaining in deck
    n_blacks: Integer number of black cards remaining in deck

    Outputs:
    policy: dictionary with 
        keys: Tuple representing state of game(n_reds, n_blacks)
        values:
            Dictionary with the following key value pairs:
                "expected_value": The expected value with optimal play for state.
                "to_play": Boolean optimal policy for game play at this state.
                "winnings": Integer number of winnings at this state in the game.

    """
    expected_value(n_reds, n_blacks)
    play = {}
    for key in EXPECTED_VALUES:
        winnings = (n_reds - key[0]) - (n_blacks - key[1])
        to_play = winnings < EXPECTED_VALUES[key] + winnings
        play.update(
            {
                key: {
                    "expected_value": EXPECTED_VALUES[key] + winnings,
                    "to_play": to_play,
                    "winnings": winnings
                }
            }
        )
    return play

def simulate_play(n_reds, n_blacks, policy):
    """  Simulates a game using the given policy.
    Inputs:
    n_reds: Integer number of red cards remaining in deck
    n_blacks: Integer number of black cards remaining in deck
    policy: dictionary with 
        keys: Tuple representing state of game(n_reds, n_blacks)
        values:
            Dictionary with the following key value pairs:
                "to_play": Boolean optimal policy for game play at this state.
                "winnings": Integer number of winnings at this state in the game. 
    """

    rem_reds = n_reds
    rem_blacks = n_blacks

    state = (n_reds, n_blacks)
    to_play = policy[state]["to_play"]

    while policy[state]["to_play"]:
        if np.random.rand(1) < rem_reds / (rem_reds + rem_blacks):
            rem_reds -= 1
        else:
            rem_blacks -= 1
        state = (rem_reds, rem_blacks)
    return policy[state]["winnings"]


def p_arrival(n_reds, n_blacks, policy, p=1):
    test_key = (n_reds, n_blacks)
    assert (test_key in policy.keys()), f"Solution was not performed for {test_key}"
    if "p" in policy[test_key].keys():
        policy[test_key]["p"] += p
        policy[test_key]["n_paths"] += 1

    else:
        policy[test_key].update({"p": p})
        policy[test_key].update({"n_paths": 1})

    if policy[test_key]["to_play"]:
        if n_reds > 0:
            p_red = n_reds / (n_reds + n_blacks)
            p_arrival(n_reds - 1, n_blacks, policy, p * p_red)
        if n_blacks > 0:
            p_black = n_blacks / (n_reds + n_blacks)
            p_arrival(n_reds, n_blacks - 1, policy, p * p_black)


def p_state(n_reds, n_blacks, policy):
    map = {(n_reds, n_blacks): 1}        
    for i in range(n_reds, -1, -1):
        for j in range(n_blacks, -1, -1):
            if (i, j) not in map.keys():
                map.update({(i, j): 0})

            if (i + 1, j) in policy.keys():
                if policy[i + 1, j]["to_play"]:
                    map[(i, j)] += (i+1) / (i+j+1) * map[(i + 1, j)]

            if (i, j + 1) in policy.keys():
                if policy[i, j + 1]["to_play"]:
                    map[(i, j)] += (j + 1) / (i + j + 1) * map[(i, j + 1)]
    for key in map:
        policy[key].update({"p": map[key]})


def plot_heatmap(n_reds, n_blacks, policy, filename=None):
    plt.figure(figsize=(n_blacks, n_reds))
    matrix = np.zeros([n_reds + 1, n_blacks + 1])
    winnings = np.zeros([n_reds + 1, n_blacks + 1])
    terminals = []
    for state in policy:
        if "p" in policy[state].keys() and policy[state]["p"] > 0:
            i = state[0]
            j = state[1]
            matrix[i, j] = policy[state]["p"]
            winnings[i, j] = policy[state]["winnings"]

            if policy[state]["to_play"]:
                color = "w"
                text = f"{matrix[i, j]:.1%}"
                fs = 12
            else:
                color = "r"
                text = f"{matrix[i, j]:.1%}\n${winnings[i, j]:.0f}"
                fs = 14
                terminals.append(
                    {
                        "p": policy[state]["p"],
                        "winnings": policy[state]["winnings"],
                        "state": state
                    }
                )
            plt.text(j, i, text, va="center", ha="center", color=color, fontsize=fs)
    plt.imshow(matrix, aspect="auto")
    plt.xlabel("Number of Black Cards Remaining")
    plt.ylabel("Number of Red Cards Remaining")
    if filename:
        os.makedirs(os.path.dirname(filename), exist_ok=True)
        plt.tight_layout()
        plt.savefig(filename)
        plt.clf()
    else:
        plt.show()
    return terminals

#%%
ns = []
ts = []
for n in range(5, 53):
    n_reds = n
    n_blacks = n
    fn_heatmap = os.path.join("src", "imgs", "heatmap", f"{n}_cards.png")
    fn_bar = os.path.join("src", "imgs", "bar", f"{n}_cards.png")
    fn_data = os.path.join("src", "g_data", f"{n}_cards.p")
    
    if os.path.exists(fn_data):
        with open(fn_data, "rb") as fin:
            data = pickle.load(fin)
        ns.append(n)
        ts.append(data["calculation_duration"])
    else:
        tstart = time()
        policy = determine_optimal_play(n_reds, n_blacks)
        p_state(n_reds, n_blacks, policy)
        terminals = plot_heatmap(n_reds, n_blacks, policy, fn_heatmap)
        tdelt = time() - tstart
        ns.append(n)
        ts.append(tdelt)
        print(f"Calculating {n_reds} Reds and {n_blacks} Blacks took {tdelt:.1f} seconds")
        
        ps = np.array([i["p"] for i in terminals])
        winnings = np.array([i["winnings"] for i in terminals])

            
        ev = np.dot(ps, winnings)
        plt.figure(figsize=(10,6))
        plt.bar(winnings, ps, width=0.5)
        plt.xticks([i for i in set(winnings)], [f"${i}" for i in set(winnings)])
        plt.xlabel("Winnings")
        plt.ylabel("Probability of Result")
        plt.vlines(ev, 0, ps.max(), color="r", linestyle="--", label=f"$ {ev:.2f} Expected Value")
        plt.xlim(left=-0.5, right=15)
        os.makedirs(os.path.dirname(fn_bar), exist_ok=True)
        plt.legend(frameon=False)
        plt.tight_layout()
        plt.savefig(fn_bar)
        plt.clf()
        plt.cla()
        os.makedirs(os.path.dirname(fn_data), exist_ok=True)

        with open(fn_data, "wb") as fout:
            pickle.dump(
                {
                    "winnings": winnings,
                    "ps": ps,
                    "policy": policy,
                    "calculation_duration": tdelt
                },
                fout
            )

        
# %%
