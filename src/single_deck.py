from calculations import determine_optimal_play
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
import seaborn as sns
import os

def plot_winnings(policy, n_reds, n_blacks):
    to_play = np.zeros([n_reds + 1, n_blacks + 1])
    winnings = np.zeros([n_reds + 1, n_blacks + 1])


    for key in policy:
        i = key[0]
        j = key[1]
        to_play[i, j] = policy[key]["to_play"]
        winnings[i, j] = policy[key]["winnings"]


    to_play = to_play.astype(bool)
    winnings[np.logical_not(to_play)] = winnings.min() - 1

    plt.figure(figsize=(n_reds, n_blacks))
    plt.imshow(winnings[::-1,::-1], aspect="auto")

    max_value = winnings.max()
    for i, row in enumerate(winnings[::-1,::-1]):
        for j, value in enumerate(row):
            if value != winnings.min():
                plt.text(j, i, f"{value:.0f}", ha="center", color="white", fontweight="bold")

    plt.xlabel("Number of Black Cards Drawn")
    plt.xticks(np.arange(0, n_blacks + 1))
    plt.yticks(np.arange(0, n_reds + 1))
    plt.xlim(-.5, n_blacks + .5)
    plt.ylim(-.5, n_reds + .5)

    plt.ylabel("Number of Red Cards Drawn")
    plt.title("Winnings at each state")
    os.makedirs("src/imgs", exist_ok=True)
    plt.savefig("src/imgs/winnings_heatmap.png")

def plot_expected_values(policy, n_reds, n_blacks):
    expected_values = np.zeros([n_reds + 1, n_blacks + 1])
    to_play = np.zeros([n_reds + 1, n_blacks + 1])


    for key in policy:
        i = key[0]
        j = key[1]
        expected_values[i, j] = policy[key]["expected_value"]
        to_play[i, j] = policy[key]["to_play"]


    to_play = to_play.astype(bool)
    expected_values[np.logical_not(to_play)] = -1

    plt.figure(figsize=(n_reds, n_blacks))
    plt.imshow(expected_values[::-1,::-1], aspect="auto")

    max_value = expected_values.max()
    for i, row in enumerate(expected_values[::-1,::-1]):
        for j, value in enumerate(row):
            if value >= 0:
                plt.text(j, i, f"{value:.1f}", ha="center", color="white", fontweight="bold")

    plt.xlabel("Number of Black Cards Drawn")
    plt.xticks(np.arange(0, n_blacks + 1))
    plt.yticks(np.arange(0, n_reds + 1))
    plt.xlim(-.5, n_blacks + .5)
    plt.ylim(-.5, n_reds + .5)

    plt.ylabel("Number of Red Cards Drawn")
    plt.title("Expected Values at each state")
    os.makedirs("src/imgs", exist_ok=True)
    plt.savefig("src/imgs/ev_heatmap.png")

if __name__ == "__main__":
    n_reds = 26
    n_blacks = 26
    policy = determine_optimal_play(n_reds, n_blacks)
    plot_winnings(policy, n_reds, n_blacks)
    plot_expected_values(policy, n_reds, n_blacks)